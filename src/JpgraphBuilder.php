<?php

namespace Drupal\jpgraph_bar;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 *
 */
class JpgraphBuilder {

  /**
   * @var \Graph
   */
  protected $graph;

  /**
   * @var \Drupal\jpgraph_bar\GraphCreatorInterface
   */
  protected $graphCreator;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\jpgraph_bar\GraphCreatorInterface $graph_creator
   */
  public function __construct(ConfigFactoryInterface $config_factory, GraphCreatorInterface $graph_creator) {
    $this->config = $config_factory->get('jpgraph_bar.configuration');
    $this->graphCreator = $graph_creator;
    $this->graph = $this->graphCreator->createGraph();
  }

  /**
   * @return void
   */
  public function build(EntityInterface $entity) {
    $str = strip_tags(Html::decodeEntities($entity->body->value));
    $ignore_words = $this->config->get('ignore_words');
    $ignore_words_regex = preg_replace(['/^[,\s]+|[,\s]+$/', '/[,\s]+/'], ['', '\b|\b'], $ignore_words);
    if ($ignore_words_regex) {
      $ignore_words_regex = '\b' . $ignore_words_regex . '\b';
      if (function_exists('mb_eregi_replace')) {
        mb_regex_encoding('UTF-8');
        $words_removed = mb_eregi_replace($ignore_words_regex, '', $str);
      }
      else {
        $ignore_words_regex = '/' . $ignore_words_regex . '/i';
        $words_removed = preg_replace($ignore_words_regex, '', $str);
      }
    }

    /**
     * @link https://yanyy.medium.com/how-to-calculate-keyword-frequency-and-draw-the-bar-chart-in-php-e66a72163a6e
     */
    $str = mb_strtolower($words_removed);
    $words = str_word_count($str, 1);
    $keywords = array_count_values($words);
    arsort($keywords);
    $keywords = array_splice($keywords, 0, 5);
    $total = count($words);
    $frequency = [];
    foreach ($keywords as $key => $value) {
      $frequency[$key] = number_format(($value / $total) * 100, 2);
    }

    $this->graph->SetScale("textlin");

    $this->graph->ygrid->SetColor('gray');
    $this->graph->ygrid->SetFill(false);
    $this->graph->yaxis->HideLine(false);
    $this->graph->yaxis->HideTicks(false, false);
    $this->graph->yaxis->title->Set("%");

    $this->graph->xaxis->SetTickLabels(array_keys($frequency));

    $this->graph->title->Set("Keyword frequency");

    // create the bar plot
    $barPlot = $this->graphCreator->createBarPlot(array_values($frequency));
    // add the bar plot to the graph
    $this->graph->Add($barPlot);

    $barPlot->SetColor("white");
    $barPlot->SetFillColor("orange");
    // display the graph
    $img_catalog = $this->config->get('img_catalog') ?: 'jpgraph';
    $fileName = 'public://' . $img_catalog . '/' . $entity->id() . '.png';
    $this->graph->Stroke($fileName);

  }

}
