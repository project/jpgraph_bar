<?php

namespace Drupal\jpgraph_bar\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Jpgraph Image block.
 *
 * @Block(
 *   id = "jpgraph_image_block",
 *   admin_label = @Translation("Jpgraph image block"),
 * )
 */
class JpgraphImageBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $node_id = $this->routeMatch->getParameter('node')->id();

    $config = $this->configFactory->get('jpgraph_bar.configuration');
    $img_catalog = $config->get('img_catalog') ?: 'jpgraph';
    if (file_exists('public://' . $img_catalog . '/' . $node_id . '.png')) {
      $build = [
        '#theme' => 'image',
        '#width' => 600,
        '#height' => 400,
        '#uri' => 'public://' . $img_catalog . '/' . $node_id . '.png',
        '#alt' => $this->t('Keyword frequency in article by jpgraph.'),
        '#title' => $this->t('Keyword frequency in article by jpgraph.'),
        '#cache' => [
          'contexts' => ['url'],
        ],
      ];
    }

    return $build;
  }

}
