<?php

namespace Drupal\jpgraph_bar;

use Drupal\Core\Config\ConfigFactoryInterface;


class GraphCreator implements GraphCreatorInterface {

  const PATH_TO_LIB = '/../lib/jpgraph';

  /**
   * @var string
   */
  protected $root;

  /**
   * @param string $root
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(string $root, ConfigFactoryInterface $config_factory) {
    $this->root = $root;
    $config = $config_factory->get('jpgraph_bar.configuration');
    $path_to_lib = $config->get('path_to_lib');
    if (!$path_to_lib) {
      $path_to_lib = self::PATH_TO_LIB;
    }
    $this->requireOnce($this->root . $path_to_lib, 'jpgraph.php');
    $this->requireOnce($this->root . $path_to_lib, 'jpgraph_bar.php');
  }

  /**
   * @param string $path
   * @param string $file
   *
   * @return void
   */
  private function requireOnce(string $path, string $file) {
    if (file_exists($path . '/src/' . $file)) {
      require_once $path . '/src/' . $file;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createGraph(): \Graph {
    return new \Graph(600, 400);
  }

  /**
   * {@inheritdoc}
   */
  public function createBarPlot(array $frequency): \BarPlot {
    return new \BarPlot($frequency);
  }

}
