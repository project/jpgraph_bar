<?php

namespace Drupal\jpgraph_bar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\jpgraph_bar\GraphCreator;

class JpgraphBarConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['jpgraph_bar.configuration'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jpgraph_bar_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('jpgraph_bar.configuration');

    $form['path_to_lib'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to library'),
      '#description' => $this->t('Specify the absolute path where the JpGraph library is installed. For the default example, we use the lib directory \'/../lib/jpgraph\' located relative to the app_root directory at the same level as the vendor directory.'),
      '#default_value' => $config->get('path_to_lib') ?: GraphCreator::PATH_TO_LIB,
      '#required' => TRUE,
    ];

    $form['ignore_words'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Strings to Remove'),
      '#default_value' => $config->get('ignore_words'),
      '#description' => $this->t('Words to strip out of the JpGraph Bar, separated by commas.'),
    ];

    $form['img_catalog'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to the directory for saving images'),
      '#description' => $this->t('Images will be stored in the specified directory nested in the specified directory \'public://\''),
      '#default_value' => $config->get('img_catalog') ?: 'jpgraph',
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('jpgraph_bar.configuration')
      ->set('path_to_lib', $form_state->getValue('path_to_lib'))
      ->set('ignore_words', $form_state->getValue('ignore_words'))
      ->set('img_catalog', $form_state->getValue('img_catalog'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
