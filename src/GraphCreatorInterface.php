<?php

namespace Drupal\jpgraph_bar;

use Drupal\Core\Config\ImmutableConfig;

interface GraphCreatorInterface {

  /**
   * @return \Graph
   */
  public function createGraph(): \Graph;

  /**
   * @param array $frequency
   *
   * @return \BarPlot
   */
  public function createBarPlot(array $frequency): \BarPlot;

}
